mod index {
    use generational_arena::Index;

    #[derive(Clone, Copy, PartialEq, Eq, Debug)] 
    pub struct EdgeIdx(pub Index);

    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    pub struct FaceIdx(pub Index);

    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    pub struct VertIdx(pub Index);
}

mod geometry {
    use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub struct Point2 { pub x: f64, pub y: f64, }

    impl Add<Point2> for Point2 {
        type Output = Self;
        fn add(self, rhs: Self) -> Self { Self { x: self.x + rhs.x, y: self.y + rhs.y, } }
    }

    impl AddAssign<Point2> for Point2 {
        fn add_assign(&mut self, rhs: Point2) {
            self.x += rhs.x;
            self.y += rhs.y;
        }
    }

    impl Sub<Point2> for Point2 {
        type Output = Self;
        fn sub(self, rhs: Point2) -> Self { Self { x: self.x - rhs.x, y: self.y - rhs.y, } }
    }

    impl SubAssign<Point2> for Point2 {
        fn sub_assign(&mut self, rhs: Point2) {
            self.x -= rhs.x;
            self.y -= rhs.y;
        }
    }

    impl Mul<f64> for Point2 {
        type Output = Self;
        fn mul(self, rhs: f64) -> Self { Self { x: self.x * rhs, y: self.y * rhs, } }
    }

    impl MulAssign<f64> for Point2 {
        fn mul_assign(&mut self, rhs: f64) {
            self.x *= rhs;
            self.y *= rhs;
        }
    }
}

mod primitives {
    use crate::lib::{EdgeIdx, FaceIdx, VertIdx, Point2};

    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    pub struct Edge { 
        pub next: EdgeIdx, pub prev: EdgeIdx, pub twin: EdgeIdx, pub face: FaceIdx, pub origin: VertIdx, }

    #[derive(Clone, Copy, PartialEq, Debug)]
    pub struct EdgeBuilder { 
        pub next: Option<EdgeIdx>, pub prev: Option<EdgeIdx>, pub twin: Option<EdgeIdx>, 
        pub face: Option<FaceIdx>, pub origin: Option<VertIdx>, }
    impl EdgeBuilder {
        pub fn new() -> EdgeBuilder { todo!() }
    }

    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    pub struct Face { pub incident_edge: EdgeIdx, }

    #[derive(Clone, Copy, PartialEq, Debug)]
    pub struct Vert { pub loc: Point2, pub outgoing_edge: EdgeIdx, }

    #[derive(Clone, Copy, PartialEq, Debug)]
    pub struct VertBuilder { loc: Point2, }
    impl VertBuilder {
        pub fn new(loc: Point2) -> VertBuilder { VertBuilder { loc } }
        pub fn build(self, outgoing_edge: EdgeIdx) -> Vert { Vert { loc: self.loc, outgoing_edge } }
    }
}

extern crate generational_arena;
use generational_arena::Arena;

use std::ops::{Index, IndexMut};

use index::*;
use geometry::*;
use primitives::*;


#[derive(Debug)]
pub struct DCEL {
    pub edges: Arena<Edge>,
    pub faces: Arena<Face>,
    pub verts: Arena<Vert>,
}

impl Index<EdgeIdx> for DCEL {
    type Output = Edge;
    fn index(&self, idx: EdgeIdx) -> &Self::Output { &self.edges[idx.0] }
}

impl IndexMut<EdgeIdx> for DCEL {
    fn index_mut(&mut self, idx: EdgeIdx) -> &mut Self::Output { &mut self.edges[idx.0] }
}

impl Index<FaceIdx> for DCEL {
    type Output = Face;
    fn index(&self, idx: FaceIdx) -> &Self::Output { &self.faces[idx.0] }
}

impl IndexMut<FaceIdx> for DCEL {
    fn index_mut(&mut self, idx: FaceIdx) -> &mut Self::Output { &mut self.faces[idx.0] }
}

impl Index<VertIdx> for DCEL {
    type Output = Vert;
    fn index(&self, idx: VertIdx) -> &Self::Output { &self.verts[idx.0] }
}

impl IndexMut<VertIdx> for DCEL {
    fn index_mut(&mut self, idx: VertIdx) -> &mut Self::Output { &mut self.verts[idx.0] }
}

impl DCEL {
    pub fn new_edge(origin: VertIdx) -> VertIdx {
        unimplemented!()
    }

    pub fn new_face(p: Point2) -> VertIdx {
        unimplemented!()
    }

    pub fn new_vert(p: Point2) -> VertIdx {
        unimplemented!()
    }
}
